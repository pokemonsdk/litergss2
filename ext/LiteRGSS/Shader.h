#ifndef Shader_H
#define Shader_H

#include "RubyValue.h"
#include "RenderStates_BlendMode.h"

extern VALUE rb_cShader;
void Init_Shader();

#endif
